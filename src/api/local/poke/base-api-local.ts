import { BaseModel } from "../../../utils/types";

/**
 * Clase que proporciona métodos para realizar operaciones CRUD utilizando el localStorage del navegador como almacenamiento local.
 */
export class BaseApiLocal {
  modelName: string;

  /**
   * Constructor de la clase.
   * @param modelName Nombre del modelo que se utilizará para almacenar los datos en el localStorage.
   * Si no se proporciona un nombre de modelo, se utiliza el valor predeterminado "undefined".
   */
  constructor(modelName: string = "undefined") {
    this.modelName = modelName;
  }

  /**
   * Método privado utilizado para garantizar que solo haya una instancia de un conjunto específico de datos en el localStorage.
   * @param item Clave bajo la cual se almacenarán los datos.
   * @param data Datos en formato de cadena que se almacenarán.
   * @returns null
   */
  private matchItem = (item: string, data: string): null => {
    const exist = localStorage.getItem(item);
    if (!exist) {
      localStorage.setItem(item, data);
      return null;
    }
    localStorage.removeItem(item);
    return this.matchItem(item, data);
  };

  /**
   * Método que devuelve todos los datos almacenados bajo la clave `modelName: nombre del modelo` del localStorage.
   * @returns Arreglo de objetos que representan los datos almacenados.
   */
  fetchAll = () => {
    const list = JSON.parse(localStorage.getItem(`${this.modelName}s`) || "[]");
    return Array.isArray(list) ? list : [];
  };

  /**
   * Método que permite almacenar una lista de modelos en el localStorage.
   * @param listModel Lista de objetos BaseModel que se almacenarán en el localStorage bajo la clave `modelName: Nombre del modelo`.
   * @returns Arreglo de objetos que representan los datos almacenados.
   */
  listAll = (listModel?: BaseModel[]) => {
    if (listModel && listModel.length > 0) {
      this.matchItem(`${this.modelName}s`, JSON.stringify(listModel));
    }
    return this.fetchAll();
  };

  /**
   * Método que se utiliza para almacenar un modelo en el localStorage.
   * @param model Modelo que se almacenará.
   * @param action Acción a realizar: "save" para guardar o "update" para actualizar (predeterminado: "save").
   */
  store = (model: BaseModel, action: "save" | "update" = "save") => {
    let allModel = this.listAll();
    this.setModel(model);

    const index = allModel.findIndex((item) => item.name === model.name);
    if (index === -1) {
      allModel.push(model);
    } else if (action == "update") {
      allModel[index] = model;
    }
    this.listAll(allModel);
  };

  /**
   * Método que se utiliza para eliminar un modelo del localStorage.
   * @param modelName Nombre del modelo que se eliminará.
   */
  delete = (modelName: string) => {
    const allModel = this.listAll().filter((item) => item.name !== modelName);
    this.listAll(allModel);
    this.setModel(allModel[0]);
  };

  /**
   * Método que devuelve el modelo almacenado en el localStorage bajo la clave `modelName: nombre del modelo`.
   * @returns Modelo almacenado en el localStorage.
   */
  getModel = () => {
    const localModel = JSON.parse(
      localStorage.getItem(this.modelName) ?? "null"
    );
    return localModel !== "null" ? localModel : ({} as BaseModel);
  };
  /**
   * Método que se utiliza para establecer un modelo en el localStorage.
   * @param model Modelo que se almacenará.
   */
  setModel = (model: BaseModel) => {
    this.matchItem(this.modelName, JSON.stringify(model));
  };
}
