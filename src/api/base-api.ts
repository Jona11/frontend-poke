// import Axios from 'axios'
import axios from 'axios'

import type { AxiosRequestConfig, AxiosRequestHeaders } from 'axios'
import qs from 'qs'
import { AnyObject, RequestData, RequestOptions } from '../utils/types'

// ENDPOINTS
export const BASE_URL = 'https:///pokeapi.co/api/v2'
export const SIGNIN_URL = `${BASE_URL}/public/auth/local`
export const SIGNUP_URL = `${BASE_URL}/public/custom`

const headersLocales: AxiosRequestHeaders = {
  'Content-type': 'application/json',
}

export default class HttpLayer {
  private getHeaders({ auth = true, headers }: RequestData) {
    const localHeaders: AxiosRequestHeaders = { ...headers }

    if (auth)
      localHeaders.Authorization = `Bearer ${localStorage.getItem('jwt')}`

    return localHeaders
  }

  public async makeRequest<U>(
    uri: string,
    method: 'get' | 'put' | 'post' | 'delete',
    opt: RequestData = {},
  ) {
    const headers = this.getHeaders(opt)
    const httpMethod = axios[method]

    const config: AxiosRequestConfig = { headers }

    if (opt.query) {
      config.params = { ...opt.query, ...opt.query.extraQuery }

      delete config.params.extraQuery

      config.paramsSerializer = (query) =>
        qs.stringify(query, { encodeValuesOnly: true })
    }

    if (['get', 'delete'].includes(method)) {
      const { data } = await httpMethod<U>(uri, config)
      return data
    }

    const response = await httpMethod<U>(uri, opt.body, config)
    return response.data
  }

  public getCall<U>(uri: string, opt: RequestData = {}) {
    return this.makeRequest<U>(uri, 'get', opt)
  }

  public postCall<U>(uri: string, opt: RequestData = {}) {
    return this.makeRequest<U>(uri, 'post', opt)
  }

  public putCall<U>(uri: string, opt: RequestData = {}) {
    return this.makeRequest<U>(uri, 'put', opt)
  }

  public deleteCall<U>(uri: string, opt: RequestData = {}) {
    return this.makeRequest<U>(uri, 'delete', opt)
  }
}

export class CrudAPI extends HttpLayer {
  endPoint = ''
  headers = headersLocales

  constructor(endPoint: any) {
    super()
    this.endPoint = `${BASE_URL}/${endPoint}`
  }

  setJWT = () =>
    (this.headers.Authorization = `Bearer ${localStorage.getItem('jwt')}`)

  public async findAll(requestOptions?: RequestOptions): Promise<AnyObject> {
    this.setJWT()
    return this.getCall(this.endPoint, requestOptions)
    // return await makeRequest(this.endPoint, 'GET', requestOptions)
  }

  public async updateById(id: number, requestOptions?: RequestOptions) {
    this.setJWT()
    await this.putCall(this.endPoint)

    // await makeRequest(`${this.endPoint}/${id}`, 'PUT', requestOptions)
  }

  public async save(entity: any, requestOptions?: RequestOptions) {
    this.setJWT()
    if (entity.id > 0) return await this.updateById(entity.id, requestOptions)
    delete entity.id
    return await this.putCall(this.endPoint, {
      body: {
        data: entity,
      },
    })
    // return await makeRequest(this.endPoint, 'POST', requestOptions)
  }

  public async deleteById(id: number) {
    this.setJWT()

    // return await makeRequest(`${this.endPoint}/${id}`, 'DELETE')
  }

  public async findById(id: number) {
    this.setJWT()
    // return await makeRequest(`${this.endPoint}/${id}`)
  }
}
