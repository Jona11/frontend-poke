import { useEffect, useState } from "react";
import axios from "axios";
import { Pagination, PaginationProps, Select } from "antd";

import {
  Ability,
  FilterPoke,
  Poke,
  SelectOption,
  Team,
} from "../../components/Crud/Poke/resources/types";
import { openNotification } from "../../utils/notify";
import { teamAPI } from "../../api/local/poke";
import PokeTeamList from "../../components/Crud/Poke/team-list";
import { BaseModel } from "../../utils/types";

// URL base para la API de Pokémon
const baseAPI = "https://pokeapi.co/api/v2/pokemon";

const MyComponent = () => {
  // Estado para almacenar los datos de los Pokémon y los filtros aplicados
  const [data, setData] = useState({
    results: [] as Poke[],
    filters: [] as Poke[],
  });
  // Estado para almacenar las opciones de habilidades
  const [options, setSelectOption] = useState([] as SelectOption[]);
  // Estado para almacenar las opciones de tipos de Pokémon
  const [optionsTypes, setSelectOptionTypes] = useState([] as SelectOption[]);
  // Estado para almacenar los filtros de búsqueda
  const [optApi, setOpt] = useState({
    name: "",
    ability: "",
    type: "",
  });
  // Estado para los parámetros de paginación
  const [filterApi, setFilterAPI] = useState({
    limit: 10,
    offset: 1,
  } as FilterPoke);

  // Función para realizar la búsqueda de Pokémon con los filtros aplicados
  const search = () => {
    const resultSearch = data.results.filter((item) => {
      let matches = true;
      // Filtrar por nombre si existe en optApi
      if (optApi.name) {
        matches =
          matches &&
          item.name.toLowerCase().startsWith(optApi.name.toLowerCase());
      }
      // Filtrar por habilidad si existe en optApi
      if (optApi.ability) {
        matches =
          matches &&
          item.abilities.some((ability) =>
            ability.name.toLowerCase().startsWith(optApi.ability.toLowerCase())
          );
      }
      // Filtrar por tipo si existe en optApi
      if (optApi.type) {
        matches =
          matches &&
          item.types.some((type) =>
            type.name.toLowerCase().startsWith(optApi.type.toLowerCase())
          );
      }
      return matches;
    });

    setData({ results: data.results, filters: resultSearch });
  };

  // Función para obtener todos los Pokémon de la API
  const getAllPokeFromApi = async (opt?: FilterPoke) => {
    const { data: dataApi } = await axios.get(baseAPI, {
      params: opt ?? filterApi,
    });
    const pokeDetails = await Promise.all(
      // Obtener detalles de cada Pokémon
      dataApi.results.map(async (poke: Poke) => {
        const { data } = await axios.get(poke.url);
        // Añadir tipos y habilidades a cada Pokémon
        poke.types = data.types.map((item: { type: { name: any } }) => ({
          name: item.type.name,
        }));
        poke.img = data.sprites.other.home.front_default;
        poke.abilities = data.abilities.map(
          (item: { ability: { name: any } }) => ({
            name: item.ability.name,
          })
        );
        return poke;
      })
    );

    setData({
      results: pokeDetails,
      filters: pokeDetails,
    });
  };
  // Función para obtener todas las habilidades de Pokémon
  const getAllAbilities = async () => {
    const { data } = await axios.get(
      "https://pokeapi.co/api/v2/ability?limit=200"
    );
    const options = data.results.map((item: Ability) => ({
      value: item.name,
      label: item.name,
    }));
    setSelectOption(options);
  };
  // Función para obtener todos los tipos de Pokémon
  const getAllType = async () => {
    const { data } = await axios.get(
      "https://pokeapi.co/api/v2/type?limit=100?offset=1"
    );
    const options = data.results.map((item: Ability) => ({
      value: item.name,
      label: item.name,
    }));
    setSelectOptionTypes(options);
  };

  // Función para actualizar los parámetros de búsqueda según el tipo
  const setRequestParams = (event: Event | undefined, searchType: string) => {
    const value = !event ? "" : event.target ? event.target.value : event;
    const opt = { ...optApi, [searchType]: !value ? "" : value };
    setOpt(opt);
  };
  // Función para manejar el cambio de página en la paginación
  const setPagination: PaginationProps["onChange"] = async (
    pageNumber,
    size
  ) => {
    const nextPage = (pageNumber - 1) * size;
    setFilterAPI({ offset: nextPage, limit: size });
    await getAllPokeFromApi({ offset: nextPage, limit: size });
  };
  // Cargar los datos iniciales al montar el componente
  useEffect(() => {
    const fetchData = async () => {
      try {
        await getAllPokeFromApi();
        await getAllAbilities();
        await getAllType();
      } catch (error) {
        console.error("Error al ejecutar la función asincrónica:", error);
      }
    };

    fetchData();
  }, []);

  // Función para obtener el local team
  const localTeam = () => teamAPI.getModel();

  const removePoke = async (model: Poke) => {
    const localTeam = teamAPI.getModel();

    const newPokes = localTeam.pokes?.filter(
      (item: Poke) => item.name !== model.name
    );
    const newTeam = { name: localTeam.name, pokes: newPokes } as Team;
    teamAPI.setModel(newTeam as unknown as BaseModel);
    teamAPI.store(newTeam as unknown as BaseModel, "update");
    await getAllPokeFromApi();
    search();
  };

  // Función para agregar un Pokémon a un equipo local
  const addPoke = async (poke: Poke) => {
    const localTeam = teamAPI.getModel();

    const exist = localTeam.pokes.find((item: Poke) => item.name === poke.name);
    if (exist) {
      openNotification({ description: "Ya esta agregado!" });
    } else {
      const newTeam = {
        ...localTeam,
        pokes: [...localTeam.pokes, poke],
      } as Team;
      teamAPI.store(newTeam as unknown as BaseModel, "update");
      openNotification({ description: "Agregado!", message: "Exito!" });
      await getAllPokeFromApi();
      search();
    }
  };

  return (
    <div className="flex lg:flex-row flex-col lg:gap-0 gap-6">
      <PokeTeamList
        className="lg:w-1/5 w-full h-4/5 P-4"
        onStorePoke={addPoke}
      />
      <div className="flex lg:w-4/5 flex-col gap-6 p-4">
        <div className="text-2xl font-semibold text-blue-600 border-b pb-4 flex justify-between">
          <span>Pokemons</span>
        </div>
        <div className="flex flex-col lg:flex-row gap-4 justify-between">
          <div className="flex flex-col lg:flex-row gap-4">
            <div className="flex flex-col ">
              {
                <Select
                  options={options}
                  placeholder="Buscar por Habilidad"
                  size="large"
                  allowClear
                  showSearch
                  onChange={(value) => setRequestParams(value, "ability")}
                  filterOption={(input, option) =>
                    (option?.label ?? "").includes(input)
                  }
                  style={{ width: 200 }}
                />
              }
            </div>
            <div className="flex flex-col">
              {
                <Select
                  options={optionsTypes}
                  size="large"
                  allowClear
                  placeholder="Buscar por tipo"
                  showSearch
                  onChange={(value) => setRequestParams(value, "type")}
                  filterOption={(input, option) =>
                    (option?.label ?? "").includes(input)
                  }
                  style={{ width: 200 }}
                />
              }
            </div>
          </div>
          <div className="flex flex-col lg:flex-row">
            <input
              type="text"
              onChange={() => setRequestParams(event, "name")}
              placeholder="Search por nombre"
              className="w-full p-2 border rounded bg-white"
            />
            <button className="text-white" onClick={search}>
              Buscar
            </button>
          </div>
        </div>
        <Pagination
          className="flex justify-end"
          showQuickJumper
          total={500}
          onChange={setPagination}
        />

        <div className="flex-grow lg:grid lg:grid-cols-3 gap-6">
          {data.filters.map((poke, index) => (
            <div
              key={index}
              className={`grid grid-cols-2 cursor-pointer bg-white border rounded shadow p-4 h-60 hover:shadow-xl `}
            >
              {/*  <img
                className="h-full flex rounded-full"
                src="https://elspotsm.com/wp-content/uploads/2020/12/Pokebola-pokeball.jpg"
                alt=""
              /> */}
              <img src={poke.img} alt="" className="h-full flex rounded-full" />
              <div className="flex flex-col flex-grow">
                <span>
                  Nombre: <b>{poke.name}</b>
                </span>
                <span>Habilidades:</span>
                {poke.abilities
                  .map((item) => item.name)
                  .map((ability, index) => (
                    <b key={index}>{ability}</b>
                  ))}
              </div>
              <div className="text-xl lg:text-sm  col-span-2 grid grid-cols-2 gap-4 p-4">
                <button
                  onClick={() => addPoke(poke)}
                  className="bg-white hover:bg-blue-700 flex justify-center hover:text-white border p-2 shadow-lg  text-center text-blue-800 border-blue-800 rounded-lg"
                >
                  <span className="flex lg:hidden">+</span>
                  <span className="hidden lg:flex">Agregar</span>
                </button>
                {localTeam().pokes.find(
                  (item: Poke) => item.name === poke.name
                ) && (
                  <button
                    onClick={() => removePoke(poke)}
                    className="border-red-500 bg-white flex justify-center text-red-500 hover:bg-red-500 hover:text-white"
                  >
                  <span className="hidden lg:flex">Eliminar</span>
                  <span className="flex lg:hidden">X</span>
                  </button>
                )}
              </div>
            </div>
          ))}
          {data.filters.length === 0 && <div> Ningun Resultado </div>}
        </div>
      </div>
    </div>
  );
};

export default MyComponent;
