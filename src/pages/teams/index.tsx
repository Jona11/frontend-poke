import PokeTeamCrud from "../../components/Crud/Poke/team";
import { TabModel } from "../../utils/types";


const columns = [
  {
    title: "Tab1",
    fields: [
      {
        field: "name",
        tag: "text",
        title: "Nombre",
      },
    ],
  },
  {
    title: "Tab2",
    fields: [
      {
        field: "pokes",
        tag: "table",
        title: "Pokes",
      },
    ],
  },
] as TabModel[];

const TeamsPage = () => {
  return (
    <div>
       <PokeTeamCrud />
      {/* <CustomTable api={api} columns={columns} /> */}
    </div>
  );
};

export default TeamsPage;
