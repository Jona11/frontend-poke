import { Outlet, Link, useLocation } from "react-router-dom";

export default function Root() {
  return (
    <>
      <div className="flex flex-x gap-4 min-h-screen bg-gray-100 text-gray-700 ">
        <div id="sidebar" className="pb-8 bg-gray-700 w-140 flex flex-col">
          <span className="font-semibold text-white text-2xl border-b py-5 px-12 ">Poke Api</span>
          <nav className="flex flex-col p-4 ">
            <Link className={`px-4 py-3 hover:bg-gray-200 rounded text-white w-100 ${useLocation().pathname === '/teams' ? 'bg-gray-200 text-violet-800' : '' }`} to={`teams`}>Teams</Link>
            <Link className={`px-4 py-3 hover:bg-gray-200 rounded text-white w-100 ${useLocation().pathname === '/pokes' ? 'bg-gray-200 text-violet-800' : '' }`} to={`pokes`}>Pokes</Link>
          </nav>
        </div>
        <div id="detail" className="flex flex-col text-gray-700 py-8 min-h-screen flex-grow">
          <Outlet />
        </div>
      </div>
    </>
  );
}
