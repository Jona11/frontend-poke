import { useEffect, useState } from "react";
import { BaseModel, TabModel } from "../../../utils/types";
import { ApiTeam } from "../../Poke/Team/resources/api";
import { Column } from "../Field";

interface Props {
  api: ApiTeam;
  className?: string;
  model?: BaseModel;
  columns: TabModel[];
}

const CustomTable = (props?: Props) => {
  const { api, model, className, columns } = props;

  const [showFields, setShowFields] = useState([] as Column[]);
  const [state, setState] = useState({
    data: [] as BaseModel[],
    fields: [] as string[],
  });

  const setData = () => {
    const items = api.listAllLocal();
    setState({
      ...state,
      data: items,
    });
  };

  const matchFields = async () => {
    const fields: Column[] = [];
    await columns.map(async (tab) => {
      await tab.fields.filter(async (field) => {
        if (!field.hiddenInTable) {
          if (!field.sort) field.sort = { active: false, dir: "asc" };
          if (!field.sort.dir) field.sort.dir = "asc";
          if (typeof field.visible === "undefined") field.visible = false;
          field.sort.active = false;
          fields.push(field);
        }
      });
    });
    setShowFields(fields);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setData();
        await matchFields();
      } catch (error) {
        console.error("Error al ejecutar la función asincrónica:", error);
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      {state.data.map((item, id) => (
        <div key={id}>
          {showFields.map((field, index) => (
            <div key={index} className="flex flex-col">
              {typeof item[field.field] === "string" && (
                <span>
                  {field.title}: {item[field.field]}
                </span>
              )}
              <span>
                {typeof Array.isArray(item[field.field]) && (
                  <span>
                    {field.title}:{" "}
                    {item[field.field]
                      ? Object.values(item[field.field]).length
                      : 0}{" "}
                  </span>
                )}
              </span>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

export default CustomTable;
