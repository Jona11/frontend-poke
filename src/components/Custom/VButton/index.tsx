const CustomButton = (props) => {
  const { label, onSave } = props;
  return (
    <button onClick={onSave} className="rounded-lg p-2 text-white ">
      {label ?? "Click me!"}
    </button>
  );
};

export default CustomButton;
