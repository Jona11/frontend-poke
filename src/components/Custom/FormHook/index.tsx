import { useForm, SubmitHandler } from "react-hook-form";
import { TabModel } from "../../../utils/types";
import { Tabs } from "antd";
import TabPane from "antd/es/tabs/TabPane";

type Inputs = {
  [x: string]: unknown;
};

const storeData = (data: Inputs, disabledSaved?: boolean) => {
  console.log(data);
};

const CustomFormHook = (props: {
  columns: TabModel[];
  disabledSaved?: boolean;
  api?: Object;
}) => {
  const { columns: tabs, disabledSaved } = props;
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = (data) =>
    storeData(data, disabledSaved);
  //console.log(watch("email")); // watch input value by passing the name of it

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="flex flex-col min-h-screen gap-4 bg-gray-700 items-center justify-center w-full"
    >
      <div className="shadow-xl rounded-lg bg-white text-gray-700 flex-y w-1/2">
        {/* header form */}
        <header className="flex flex-row-reverse p-3 border-b">
          <button
            type="submit"
            className="flex rounded px-3 py-2 bg-gray-900 text-white"
          >
            Guardar
          </button>
        </header>
        {/* body form */}
        <div className="gap-4 px-6 pb-10 pt-4 flex flex-col">
          {/* header tabs */}

          <div>
            <Tabs defaultActiveKey="0" className="space-y-4">
              {tabs.map((tab: TabModel, index: number) => (
                <TabPane tab={tab.title} key={index}>
                  {tab.fields.map((field, index) => (
                    <div key={index} className="flex flex-col gap-1 pb-6">
                      <span> {field.title} </span>
                      <input
                        type={field.type}
                        {...register(field.field as string, {
                          required: field.required === false ? false : true,
                        })}
                        className="p-2 bg-white border rounded-lg text-gray-700"
                      />
                      {/* errors will return when field validation fails  */}
                      {errors[field.field] && (
                        <span className="text-red-600 text-xs font-light">
                          This field: {field.field} is required
                        </span>
                      )}
                    </div>
                  ))}
                </TabPane>
              ))}
            </Tabs>
          </div>
        </div>
      </div>
    </form>
  );
};

export default CustomFormHook;
