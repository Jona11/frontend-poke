import { useState } from "react";

const CustomField = (props) => {
  const { label, name, placeholder, isNorequired } = props;
  const [property, setProperty] = useState("");
  const handleInput = (event) => {
    const { value } = event.target;
    setProperty(value);
    props.model(event);
  };

  return (
    <div className="flex text-gray-500 flex-col gap-1">
      <div className="font-light">{label ? `${label} :` : ''}
      <span className={ !isNorequired ? 'text-red-500': 'hidden' } >*</span>
      </div>
      <input
        value={property}
        placeholder={placeholder || "Complete el Campo"}
        name={name}
        required={!isNorequired}
        onChange={handleInput}
        className="bg-white text-gray-500 focus:text-gray-600 border focus:ring-4 focus:ring-gray-300/30 focus:outline-none rounded p-2"
      />
    </div>
  );
}

export default CustomField
