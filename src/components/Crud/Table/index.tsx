import { useEffect, useState } from "react";
import { teamAPI } from "../../../api/local/poke";
import { Team } from "../Poke/resources/types";


const TeamTable = () => {
  const [team, setTeam] = useState({} as Team);
  const [teams, setTeams] = useState([] as Team[]);

  const getAllData = async () => {
    const teams = teamAPI.listAll();
    setTeams(teams);
    setTeam(teams[0]);
  };



  useEffect(() => {
    const fetchData = async () => {
      try {
        getAllData();
      } catch (error) {
        console.error("Error al ejecutar la función asincrónica:", error);
      }
    };

    fetchData();
  }, []);

  return (
   <div></div>
  );
};

export default TeamTable;
