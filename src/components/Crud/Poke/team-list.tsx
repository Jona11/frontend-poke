import { useEffect, useState } from "react";
import { teamAPI } from "../../../api/local/poke";
import { Team } from "./resources/types";
import { BaseModel } from "../../../utils/types";

// Componente funcional PokeTeamList que muestra una lista de equipos y permite seleccionar uno
const PokeTeamList = (props?) => {
  // Props opcionales: className (clases CSS) y onStorePoke (función para almacenar un Pokemon en el equipo)
  const { className, onStorePoke } = props;
  
  // Estado para almacenar el equipo seleccionado y la lista de equipos
  const [team, setTeam] = useState({} as Team);
  const [teams, setTeams] = useState([] as Team[]);

  // Función para obtener todos los datos de los equipos
  const getAllData = async () => {
    const teams = teamAPI.listAll(); // Obtiene todos los equipos
    setTeams(teams); // Actualiza el estado de la lista de equipos
    const currentTeam = teamAPI.getModel() || teams[0]; // Obtiene el equipo actual seleccionado o el primer equipo si no hay ninguno seleccionado
    setTeam(currentTeam); // Actualiza el estado del equipo seleccionado
    teamAPI.setModel(currentTeam as unknown as BaseModel); // Establece el equipo actual como el modelo en la API de equipos
  };

   // Función para establecer el equipo actualmente seleccionado
   const setCurrentTeam = (team: Team) => {
    setTeam(team); // Actualiza el estado del equipo seleccionado
    teamAPI.setModel(team as unknown as BaseModel); // Establece el equipo seleccionado como el modelo en la API de equipos
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getAllData(); // Obtiene todos los datos de los equipos
      } catch (error) {
        console.error("Error al obtener datos"); // Manejo de errores en caso de falla al obtener datos
      }
    };

    fetchData(); // Ejecuta la función para obtener datos al montar el componente o al cambiar la función para almacenar un Pokemon
  }, [onStorePoke]); // Dependencia: onStorePoke (función para almacenar un Pokemon en el equipo)


  return (
    <div className={`flex flex-col bg-white gap-6 p-6 ${className}`}>
      <span> Selecciona Equipo </span>
      <div className="flex flex-col gap-4 text-xl overflow-y-scroll h-50 w-120">
        {/* Mapea sobre la lista de equipos para mostrar cada uno */}
        {teams.map((tm, i) => (
          <div
            key={i}
            onClick={() => setCurrentTeam(tm)} // Manejador de evento para seleccionar un equipo al hacer clic
            className={`flex flex-col hover:shadow-lg hover:cursor-pointer rounded-lg w-full p-3 ${
              tm.name === team.name ? "bg-gray-800 text-white " : "border"
            }`}
          >
            <div className="flex justify-between w-120">
              <span className="text-sm">Nombre:</span>
              <span className="text-sm">Total Pokes:</span>
            </div>
            <div className="flex justify-between">
              <span>{tm.name}</span>
              <span> {tm.pokes?.length || 0} </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default PokeTeamList;
