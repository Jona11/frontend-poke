import { useEffect, useState } from "react";
import {
  CloseOutlined,
  EditOutlined,
  EyeOutlined,
  InfoCircleOutlined,
  QuestionCircleOutlined,
  SaveOutlined,
} from "@ant-design/icons";
import { Popconfirm } from "antd";
import { teamAPI } from "../../../api/local/poke";
import { Poke, Team } from "./resources/types";
import { BaseModel } from "../../../utils/types";
import { openNotification } from "../../../utils/notify";
import { validateData } from "../../../utils/validator";

const PokeTeamCrud = () => {
  // Estado para el nombre del equipo nuevo
  const [teamName, setTeamName] = useState("");
  // Estado para controlar la visibilidad de la sección de detalles de equipo
  const [show, changeShow] = useState(false);
  // Estado para indicar si se está editando un equipo
  const [isActiveEdit, setEdit] = useState(false);
  // Estado para el nombre del equipo en edición
  const [teamNameEdit, setTeamEdit] = useState("");

  // Estado para el equipo seleccionado actualmente
  const [team, setTeam] = useState({ name: "", pokes: [] } as Team);
  // Estado para almacenar todos los equipos
  const [teams, setTeams] = useState([] as Team[]);
  // Estado para almacenar equipos filtrados
  const [teamsFilters, setFilters] = useState([] as Team[]);

  // Función para obtener todos los datos de los equipos
  const getAllData = async () => {
    const teams = teamAPI.listAll();
    const currentTeam = teamAPI.getModel() || teams[0];
    setTeam(currentTeam);
    setTeams(teams);
    setFilters(teams);
  };

  // Función para eliminar un equipo
  const removeTeam = async (nameTeam: string) => {
    teamAPI.delete(nameTeam);
    await getAllData();
  };

  // Función para establecer el equipo actual
  const setCurrentTeam = (team: Team) => {
    setTeam(team);
    teamAPI.setModel(team as unknown as BaseModel);
  };

  // Función para manejar la acción de editar un equipo
  const onClickEdit = (edit: boolean, model: Team) => {
    setEdit(edit);
    setCurrentTeam(model);
    setTeamEdit(model.name);
  };

  // Función para manejar la edición del nombre de equipo
  const onEditModel = (event: { target: { value: any } }) => {
    const value = !event ? null : event.target ? event.target.value : null;
    setTeamEdit(value);
  };

  // Función para actualizar un equipo editado
  const updateModel = async () => {
    const newTeam = {
      name: teamNameEdit,
      pokes: team.pokes,
    } as Team;

    const exist = teams.some((item) => item.name === teamNameEdit);
    if (!exist) {
      teamAPI.store(newTeam as unknown as BaseModel, "update");
      teamAPI.delete(team.name);
      setEdit(false);
      await getAllData();
    } else {
      openNotification({ description: "Nombre ya existente!" });
    }
  };

  // Función para buscar equipos por nombre
  const searchByName = async (event: { target: { value: any } }) => {
    const value = !event ? null : event.target ? event.target.value : null;

    if (!value) {
      await getAllData();
    } else {
      const a = teamsFilters.filter((item) =>
        item.name
          .toLocaleLowerCase()
          .startsWith((value as string).toLowerCase())
      );
      setTeams(a);
    }
  };

  // Función para almacenar un nuevo equipo
  const storeTeam = async (team: Team) => {
    const exist = teams.find((currentTeam) => currentTeam.name === team.name);
    // if no exist team name, push
    if (!exist) {
      teamAPI.store(team as unknown as BaseModel);
      await getAllData();
    } else {
      // if aready exist team name, show message error
      openNotification({ message: "Grupo ya existe" });
    }
  };

  // Función para validar la entrada de datos del equipo
  const validate = () => {
    setTeam({ name: teamName, pokes: [] });
    const fix = validateData(team).fix;
    if (fix) {
      openNotification({
        icon: <InfoCircleOutlined />,
      });
    } else {
      storeTeam(team);
    }
  };

  // Función para eliminar un Pokémon de un equipo
  const removePoke = async (teamModel: Team, model: Poke) => {
    const newPokes = teamModel.pokes?.filter(
      (item) => item.name !== model.name
    );
    const newTeam = { name: teamModel.name, pokes: newPokes };
    setTeam(newTeam);
    teamAPI.store(newTeam as unknown as BaseModel, "update");
    await getAllData();
  };

  // Función para manejar el cambio en el nombre del equipo nuevo
  const handleChange = (event) => {
    const value = event.target.value;
    setTeam({ name: value, pokes: [] });
    setTeamName(value);
  };

  // Cargar los datos iniciales al montar el componente
  useEffect(() => {
    const fetchData = async () => {
      try {
        await getAllData();
      } catch (error) {
        console.error("Error al ejecutar la función asincrónica:", error);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="flex flex-col gap-10 p-4 bg-white">
      <b className="text-blue-600 text-2xl">Mis Equipos</b>
      <div className="flex flex-row justify-between">
        <div className="flex flex-row w-full">
          <input
            type="text"
            value={teamName}
            onChange={() => handleChange(event)}
            placeholder="Introduce un nombre"
            className="p-2 border rounded bg-white"
          />
          <button className="text-white" onClick={validate}>
            Crear
          </button>
        </div>
        <div className="flex flex-row">
          <input
            type="text"
            onChange={searchByName}
            placeholder="Search por nombre"
            className="w-60 p-2 border-b-2 rounded bg-white"
          />
          <button className="text-gray-800 bg-white border">Buscar</button>
        </div>
      </div>
      <div className="flex justify-between">
        <span> Selecciona Equipo </span>
      </div>
      <div className="flex flex-col gap-12 text-xl overflow-y-scroll h-50">
        {teams.map((tm, i) => (
          <div
            key={i}
            onClick={() => setCurrentTeam(tm)}
            className={`flex border-2 flex-col hover:shadow-lg hover:cursor-pointer rounded w-full ${
              tm.name === team.name ? "border-blue-600" : ""
            }`}
          >
            <div
              className={`flex justify-between p-4 border-b ${
                tm.name === team.name ? "text-blue-00 " : ""
              }`}
            >
              <div className="text-sm font-semibold flex flex-row items-center gap-4">
                Nombre:
                {(!isActiveEdit || team.name !== tm.name) && <b> {tm.name} </b>}
                {isActiveEdit && team.name === tm.name && (
                  <div className="flex flex-row gap-2">
                    <input
                      type="text"
                      value={teamNameEdit}
                      className="rounded bg-white p-2 border"
                      onChange={() => onEditModel(event)}
                    />
                    <SaveOutlined onClick={updateModel} className="text-2xl" />
                  </div>
                )}
              </div>
              <div className="flex flex-row gap-5 text-2xl">
                <EditOutlined onClick={() => onClickEdit(!isActiveEdit, tm)} />
                <EyeOutlined onClick={() => changeShow(!show)} />
                <Popconfirm
                  title="Eliminar"
                  cancelText="Cancelar"
                  description="Esta seguro que desea eliminar este lemento?"
                  icon={<QuestionCircleOutlined style={{ color: "red" }} />}
                  onConfirm={() => removeTeam(tm.name)}
                >
                  {/* <Button danger>Eliminar</Button> */}
                  <CloseOutlined className="text-red-600" />
                </Popconfirm>
              </div>
            </div>
            {show && tm.name === team.name && (
              <div className="p-2 flex-col flex md:grid md:grid-cols-4 gap-4 text-sm">
                <b className="col-span-4"> {tm.pokes?.length ?? 0} Pokes: </b>
                {tm.pokes?.map((poke, id) => (
                  <div key={id} className="">
                    <div
                      className={`md:grid hidden grid grid-cols-2 bg-white relative cursor-pointer border rounded shadow p-4 h-48 hover:shadow-xl `}
                    >
                      {/* <img
                        className="h-full flex rounded-full"
                        src="https://elspotsm.com/wp-content/uploads/2020/12/Pokebola-pokeball.jpg"
                        alt=""
                      /> */}
                      <img
                        src={poke.img}
                        alt=""
                        className="h-full flex rounded-full"
                      />
                      <div className="flex flex-col flex-grow">
                        <span>
                          Nombre: <b>{poke.name}</b>
                        </span>
                        <span>Habilidades:</span>
                        {poke.abilities
                          .map((item) => item.name)
                          .map((ability, index) => (
                            <b key={index}>{ability}</b>
                          ))}
                      </div>
                      <Popconfirm
                        title="Eliminar"
                        cancelText="Cancelar"
                        description="Esta seguro que desea eliminar este lemento?"
                        icon={
                          <QuestionCircleOutlined style={{ color: "red" }} />
                        }
                        onConfirm={() => removePoke(tm, poke)}
                      >
                        {/* <Button danger>Eliminar</Button> */}
                        <button className="text-gray-500 bg-white col-span-2 border-2 p-2 shadow-lg text-sm font-bold text-2xl text-center border-gray-800 rounded-lg">
                          Remover
                        </button>
                      </Popconfirm>
                    </div>
                    <div className="md:hidden flex justify-between border p-2">
                      <span>
                        Nombre: <b>{poke.name}</b>
                      </span>
                      <Popconfirm
                        title="Eliminar"
                        cancelText="Cancelar"
                        description="Esta seguro que desea eliminar este lemento?"
                        icon={
                          <QuestionCircleOutlined style={{ color: "red" }} />
                        }
                        onConfirm={() => removePoke(tm, poke)}
                      >
                        <CloseOutlined />
                      </Popconfirm>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default PokeTeamCrud;
