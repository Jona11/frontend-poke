export interface PokeModel {
    name: string;
    weight: number;
    base_experience: number;
    abilities: [];
  }
  
  export interface SelectOption {
    value: string;
    label: string;
  }
  
  export interface Ability {
    name: string;
  }
  
  export interface FilterPoke {
    offset: number; // number page
    limit: number; // size result
  }
  export interface Team {
    name: string;
    pokes?: Poke[];
  }
  
  export interface Poke {
    name: string;
    url: string;
    img: string;
    abilities: Ability[];
    types: Ability[];
  }
  