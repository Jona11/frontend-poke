import React from "react";
import ReactDOM from "react-dom/client";
import "./App.css";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root from "./App";
import ErrorPage from "./pages/error-page";
import Pokes from "./pages/pokes";
import TeamsPage from "./pages/teams";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "pokes",
        element: <Pokes />,
      },
      {
        path: "teams",
        element: <TeamsPage />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    {/*  <App /> */}
      <RouterProvider router={router} />
  </React.StrictMode>
);
