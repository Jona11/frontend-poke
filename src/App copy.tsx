import CustomFormHook from "./components/Custom/FormHook";
import { TabModel } from "./utils/types";

const columns: TabModel[] = [
  {
    title: "Tab 1",
    fields: [
      {
        id: "0",
        field: "name",
        title: "Nombre",
        tag: "text",
      },
      {
        id: "1",
        field: "email",
        title: "Correo",
        type: "email",
      },
    ],
  },
  {
    title: "Tab 2",
    fields: [
      {
        id: "0",
        field: "age",
        title: "Edad",
        required: false,
      },
      {
        id: "1",
        field: "gender",
        title: "Sexo",
      },
    ],
  },
];

const api = {
  
}

export default function App() {
  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    <div className="flex flex-col min-h-screen bg-gray-200 items-center justify-center w-full">
      <CustomFormHook columns={columns} disabledSaved api={api} />
    </div>
  );
}