/**
 * Get token from localstorage
 *
 * @return The token
 */
export function getToken(): string {
  return localStorage.getItem('token') || ''
}

/**
 * Set token in localstorage
 *
 * @param token - The token to store in localstorage
 */
export function setToken(token: string): void {
  localStorage.setItem('token', token)
}

/**
 * Check if token exists in localstorage
 *
 * @returns If token extists
 */
export function hasToken(): boolean {
  return localStorage.getItem('token') !== null
}

/**
 * Remove token from localstorage
 */
export function removeToken(): void {
  localStorage.removeItem('token')
}
