export const validateData = (model: Object) => {
    const keys = Object.keys(model);
  
    const error = {
      fix: false,
      fields: [] as string[],
    };
  
    for (const key of keys) {
      if (!model[key]) {
        error.fix = true;
        error.fields.push(model[key]);
      } else {
        error.fix = false;
      }
    }
    return error;
  };
  