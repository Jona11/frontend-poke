import type { AxiosRequestHeaders } from 'axios'
import { ReactNode } from 'react';


type BaseModelField = string | number | Object | boolean;
export type AnyObject<T = unknown> = Record<string, T>;

export interface BaseModel {
  id: number;
  [x: string]: BaseModelField;
}

export interface Column<BaseModel> {
  id: string;
  title: string;
  tooltip?: string;
  visible?: boolean;
  class?: string;
  width?: string;
  emitName?: "string";
  tag?:
    | "text"
    | "date"
    | "numberToString"
    | "img"
    | "vselect"
    | "number"
    | "decimal"
    | "api"
    | "table"
    | "label"
    | "boolean"
    | "dui";
  type?: "input" | "textarea" | "password" | "number" | "date" | "email";
  minLength?: number;
  minValue?: number;
  api?: unknown;
  fields?: Column<BaseModel>[];
  required?: boolean;
  fieldUnique?: boolean;
  showForm?: boolean;
  hiddenInTable?: boolean;
  field: keyof BaseModel;
  formatter?: (data?: any, row?: BaseModel) => string;
  headerClass?: string;
  headerStyle?: Record<string, string>;
  style?: Record<string, string>;
}

export type TranslateFunction = (
  key: string | number,
  plural?: number
) => string;

export interface TabModel {
  title: string;
  fields: Column<BaseModel>[];
  classes?: string;
  autorize?: boolean;
}

export type FilterParams = AnyObject;
export type PopulateParams = string | string[] | AnyObject;
export interface PaginationParams {
  page: number;
  pageSize: number;
  withCount?: boolean;
}
export interface RequestOptions {
  auth?: boolean;
  delay?: number;
  url?: string;
  sort?: string | string[];
  filters?: FilterParams;
  populate?: PopulateParams;
  fields?: string[];
  pagination?: PaginationParams;
  extraQuery?: AnyObject;
  data?: Object;
  params?: AnyObject & { extraParams?: AnyObject };
}

export interface RequestData {
  query?: AnyObject & { extraQuery?: AnyObject }
  auth?: boolean
  headers?: AxiosRequestHeaders
  body?: AnyObject | AnyObject[] | FormData
}
