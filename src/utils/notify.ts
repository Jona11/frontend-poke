import { notification } from "antd";
import { ReactNode } from "react";

interface NotificationProps {
    message?: string;
    description?: string | ReactNode;
    icon?: ReactNode;
  }
  
export const openNotification = (props: NotificationProps) => {
    const { message, description, icon } = props;
    notification.open({
      message: message || "Error!",
      description: description || "Complete todos los campos",
      onClick: () => {
        console.log("Notificación clickeada!");
      },
      placement: "topLeft",
      icon: icon || null,
      //className: "bg-red-500",
    });
  };